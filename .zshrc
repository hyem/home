# .zshrc

# System Detection {{{
# My device hostnames with batteries
laptops=('void' 'rogue' 'nb505')
user="$USER"
hostname=$(hostname)
for host in $laptops; do
    if [ host = hostname ]; then
        laptop=true
    fi
done
# End System Detection }}}
# Basic Settings {{{
setopt histignorealldups sharehistory

# Use vim keybindings even if our EDITOR is set to emacs
bindkey -v
export EDITOR='vim'
export PAGER='less'

# Keep 9999 lines of history within the shell and save it to ~/.zsh_history:
export HISTSIZE=9999
export SAVEHIST=9999
export HISTFILE=~/.zsh_history

COMPLETION_WAITING_DOTS="true"

# End Basic Settings }}}
# Oh My Zsh {{{
# Theme Settings {{{

# Path to your oh-my-zsh installation.
export ZSH=/home/datenstrom/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
DEFAULT_USER="datenstrom"
POWERLEVEL9K_MODE='nerdfont-complete'
ZSH_THEME="powerlevel9k/powerlevel9k"

POWERLEVEL9K_DISK_USAGE_ONLY_WARNING=true

# Directory path settings
POWERLEVEL9K_SHORTEN_DIR_LENGTH=0
POWERLEVEL9K_SHORTEN_DELIMITER=" "
# remove to limit by num directories instead of letters
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
#POWERLEVEL9K_DIR_PATH_SEPARATOR="$(print_icon 'LEFT_SUBSEGMENT_SEPARATOR') %F{black}"
POWERLEVEL9K_DIR_PATH_SEPARATOR=">"

if $laptop; then
    POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs dir_writable)
    POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator vi_mode background_jobs battery disk_usage virtualenv  time)
else
    POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir virtualenv vcs dir_writable)
    POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator vi_mode background_jobs disk_usage virtualenv time)
fi

# End Theme Settings }}}
# Extended Settings {{{
# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
 HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# End Extended Settings }}}
# Plugins {{{
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode)
# End Plugins }}}
source $ZSH/oh-my-zsh.sh
# End Oh My Zsh }}}
# User Configuration {{{
export EDITOR='vim'

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias python="python3"
alias pip="pip3"
alias hy="hy3"
alias vi="vim"
alias config='/usr/bin/git --git-dir=/home/datenstrom/.cfg/ --work-tree=/home/datenstrom'
# End User Configuration }}}
# Path {{{
# Set .bin in PATH and it should be first
export PATH="$HOME/.bin/bash:$PATH"
export PYTHONPATH="$HOME/.local/bin/:$HOME/.bin/python/:${PYTHONPATH}"
# Go path, for golangs scratch space
export GOPATH=$HOME/.go
export PATH=$GOPATH/bin:$PATH
 export PATH=$HOME/bin:/usr/local/bin:$PATH
# End Path }}}
