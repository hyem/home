" .vimrc

" Basic settings and variables"{{{

filetype plugin indent on
syntax on
set encoding=utf-8
set visualbell noerrorbells " don't beep
set hlsearch incsearch      " hightlight search and incremental search
set gdefault                " global replace by default
set nowrap                  " not wrap lines
set nu                      " show line numbers
set foldlevel=1             " default foldlevel 1 to see headings
set nobackup noswapfile     " stop backup and swap files
set nocompatible ignorecase smartcase
set nocindent noautoindent nosmartindent indentexpr= "disable autoindents
set tabstop=4 shiftwidth=4 expandtab "setup default tab/shift/expand
set showmode showcmd ttyfast
set guioptions=a            " hide scrollbars/menu/tabs
let mapleader = ","
let maplocalleader = ";"
set foldmethod=marker       " sets the fold method to {{{ }}} markers
set shortmess=atI           " disable welcome screen
set listchars=tab:\|\ ,trail:·,eol:¬
set nospell                 " disable spellcheck for code
set backspace=indent,eol,start " make backspace normal
" End Basic settings and variables}}}
" neovim {{{
if !has('nvim')
    set ttymouse=xterm2
endif
" End neovim }}}
" Auto-commands {{{

aug derek
  " Remove all autocommands for the current group.
  au!
  " fold up dotfiles
  au BufRead,BufNewFile .* set foldlevel=0 textwidth=80
  " python pep-8
  au BufRead,BufNewFile *.py set ft=python textwidth=80
  if v:version > 703
    au BufRead,BufNewFile *.py set colorcolumn=80
  endif
  " .md extension is markdown
  au BufRead,BufNewFile *.md set ft=markdown foldlevel=2 wrap linebreak textwidth=0 wrapmargin=0 spell
  au BufRead,BufNewFile *.wp set ft=markdown foldlevel=2 wrap linebreak textwidth=0 wrapmargin=0 spell
  if v:version > 703
    au BufRead,BufNewFile *.md set colorcolumn=80
    au BufRead,BufNewFile *.wp set colorcolumn=80
  endif

  " Spelling on markdown
  au BufRead,BufNewFile *.md set spell
  au BufRead,BufNewFile *.go set ts=4
  " run go test on Dispatch
  au FileType go let b:dispatch = 'go build'
  " javascript tabstop 2 expandtab
  au BufRead,BufNewFile *.js set ft=javascript foldlevel=2 ts=2 sw=2 expandtab textwidth=79
  if v:version > 703
    au BufRead,BufNewFile *.js set colorcolumn=80
  endif
aug END
" End Auto-commands }}}
" Keyboard Shortcuts and remappings   "{{{

"changes with less keystroke
nnoremap ; :
" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za
"reload the .vimrc
nmap <silent> <leader>rv :source ~/.vimrc<CR>
"show spaces"
nmap <silent> <leader>s :set nolist!<CR>
"show line numbers"
nmap <silent> <leader>l :set nonu!<CR>
"wrap lines"
nmap <silent> <leader>w :set nowrap!<CR>
"hide hightlight of searches"
nmap <silent> // :nohlsearch<CR>
" Movements shortcuts {{{

" C-h/j/k/l to move between buffers
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
" Buffer switching/management, might as well use those keys for something useful
map <Right> :bnext<CR>
imap <Right> <ESC>:bnext<CR>
map <Left> :bprev<CR>
imap <Left> <ESC>:bprev<CR>
" Maximize only this window"
nmap <silent> <leader>m :only<CR>
"vertical split"
nmap <silent> <leader>v :bel :vne<CR>
"horizontal split"
nmap <silent> <leader>f :bel :new<CR>
"close viewport buffer"
nmap <silent> <leader>x :hid<CR>
" }}}
" Paste and visual paste improvments {{{
vnoremap <silent> y y`]
vnoremap <silent> p p`]
nnoremap <silent> p p`]

" vp doesn't replace paste buffer
function! RestoreRegister()
  let @" = s:restore_reg
  return ''
endfunction
function! s:Repl()
  let s:restore_reg = @"
  return "p@=RestoreRegister()\<cr>"
endfunction
vmap <silent> <expr> p <sid>Repl()
" }}}
" End Keyboard Shortcuts}}}
" Theme and Color {{{

set background=dark
set t_Co=16
colorscheme solarized
"font is Iosevka
" set noantialias
set guifont=Iosevka\ Medium\ 12
"draw vertical column at 80
if v:version > 703
  set colorcolumn=80
endif
" End Theme and Color }}}
" Quick editing  {{{

" Edit the .bashrc"
nmap <silent> <leader>eb :e ~/.bashrc<CR>
" Edit the .vimrc"
nmap <silent> <leader>ev :e ~/.vimrc<CR>
" Edit the .gitconfig"
nmap <silent> <leader>eg :e ~/.gitconfig<CR>
" Edit the .tmux.conf"
nmap <silent> <leader>et :e ~/.tmux.conf<CR>
" Edit slate configuration
nmap <silent> <leader>el :e ~/.slate<cr>
" Open a scratch file
nmap <silent> <leader>eh :e ~/scratch.txt<CR>
" Open dev tools content folder
nmap <silent> <leader>ec :e ~/.vim/bundle/solarized/colors/solarized.vim<CR>
" End Quick editing  }}}
" Plugins Management"{{{
call plug#begin('~/.vim/plugged')
" File Navigation {{{

" File sidebar
Plug 'https://github.com/scrooloose/nerdtree.git'

" Fuzzy file searching
Plug 'https://github.com/ctrlpvim/ctrlp.vim'

" End File Navigation }}}
" Status Bar {{{

" Lightweight status bar inspired by powerline
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/vim-airline/vim-airline-themes.git'

" End Status Bar }}}
" Colors / Icons {{{

" Icons for files based on patched fonts
Plug 'ryanoasis/vim-devicons'

" Awesome colors
Plug 'https://github.com/altercation/vim-colors-solarized.git'

" Add color change for indent guides
Plug 'https://github.com/nathanaelkane/vim-indent-guides.git'

" End Colors / Icons }}}
" Markers {{{

" Git based markers
Plug 'mhinz/vim-signify'

" Manually set markers
Plug 'https://github.com/kshenoy/vim-signature.git'

" End Markers }}}
" Python {{{
Plug 'klen/python-mode'
" End Python }}}
" Hylang {{{
Plug 'https://github.com/hylang/vim-hy.git'
" End Hylang }}}
" Clojure {{{

" Clojure runtime files
Plug 'https://github.com/guns/vim-clojure-static.git'

" Parentheses matching for the intense
Plug 'https://github.com/kien/rainbow_parentheses.vim.git'

" Better syntax highlighting
Plug 'https://github.com/guns/vim-clojure-highlight.git'

" Not qute a REPL
Plug 'https://github.com/tpope/vim-fireplace.git'

" End Clojure }}}
" LaTeX {{{
Plug 'lervag/vimtex'
" End LaTeX }}}
" Markdown {{{
Plug 'https://github.com/plasticboy/vim-markdown.git'
" End Markdown }}}
" Misc {{{

" Syntax Checking for many languages
" Missing: Clojure, Rust
Plug 'https://github.com/vim-syntastic/syntastic.git'

" Snippets
Plug 'SirVer/ultisnips'

" Open files at last place
Plug 'git://github.com/farmergreg/vim-lastplace.git'

" Run Commands
Plug 'https://github.com/tpope/vim-fugitive.git'

" End Misc }}}
" You Complete Me {{{
"function! BuildYCM(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
"  if a:info.status == 'installed' || a:info.force
"    !./install.py
"  endif
"endfunction

"Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM') }
" End You Complete Me }}}
call plug#end()
" End Plugins Management"}}}
" Plugins configuration"{{{

" Nerdtree "{{{
map <leader>d :execute 'NERDTreeToggle ' . getcwd()<CR>
let NERDTreeIgnore=['node_modules$[[dir]]', '\.git$[[dir]]']
"}}}
" Vim Airline {{{
set laststatus=2
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_left_sep = ''
let g:airline_right_sep = ''
" }}}
" ctrlp {{{

let g:ctrlp_map = '<c-p>'
let g:ctrlp_dotfiles = 1
let g:ctrlp_working_path_mode = 2
let g:ctrlp_match_window_bottom = 0
let g:ctrlp_match_window_reversed = 0
let g:ctrlp_custom_ignore = {
  \ 'dir':  'build/.*\|/temp/.*',
  \ 'file': '\.jar$\|\.ear$|\.zip',
  \ }
let g:ctrlp_user_command = 'git ls-files %s'
" }}}
" Vim Dispatch {{{
nnoremap <leader>gt :Dispatch<CR>
" }}}
" python-mode {{{
let g:pymode_python = 'python3'
let g:pymode_folding = 1
let g:pymode_indent = 1
let g:pymode_motion = 1
let g:pymode_trim_whitespaces = 1
" g:pymode_option ==
" setlocal complete+=t                                                        
" setlocal formatoptions-=t                                                   
" if v:version > 702 && !&relativenumber                                      
"     setlocal number                                                         
" endif                                                                       
" setlocal nowrap                                                             
" setlocal textwidth=79                                                       
" setlocal commentstring=#%s                                                  
" setlocal define=^\s*\\(def\\\\|class\\)                                     
let g:pymode_options = 1
" Docs
let g:pymode_doc = 1
let g:pymode_doc_bind = 'K'
" Use all syntax
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_slow_sync = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_lint_sort = ['E', 'C', 'I'] " Errors first 'E', after them 'C' and ...
let g:pymode_lint_cwindow = 1
let g:pymode_lint_signs = 1
let g:pymode_lint_todo_symbol = 'WW'
let g:pymode_lint_comment_symbol = 'CC'
let g:pymode_lint_visual_symbol = 'RR'
let g:pymode_lint_error_symbol = 'EE'
let g:pymode_lint_info_symbol = 'II'
let g:pymode_lint_pyflakes_symbol = 'FF'
" Code check
let g:pymode_lint = 1
let g:pymode_lint_checker = ["pep8", "pyflakes", "pylint", "mccabe"]
let g:pymode_lint_write = 1
let g:pymode_lint_unmodified = 1
let g:pymode_lint_on_fly = 0
let g:pymode_lint_message = 1
" Support virtualenv
let g:pymode_virtualenv = 1
" Rope Support
let g:pymode_rope_show_doc_bind = '<C-c>d'
" Rope completion
let g:pymode_rope_completion = 1
let g:pymode_rope_completion_bind = '<C-Space>'
let g:pymode_rope_autoimport = 1
let g:pymode_rope_autoimport_import_after_complete = 1
" Sort imports by PEP8
let g:pymode_rope_organize_imports_bind = '<C-c>ro'
" Instert import for word under cursor
let g:pymode_rope_autoimport_bind = '<C-c>ra'
" Run Code
let g:pymode_run = 1
let g:pymode_run_bind = '<leader>r'
" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<leader>b'
" }}}
" Indent Guides {{{
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 1
let g:indent_guides_color_change_percent = 10
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']
" End Indent Guides }}}
" hylang {{{
let g:hy_enable_conceal = 1
" End hylang }}}

" End Plugins configuration"}}}
