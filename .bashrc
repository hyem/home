# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# basic settings {{{
export HISTFILESIZE=999999
export HISTSIZE=999999
export HISTCONTROL=ignoredups:ignorespace
shopt -s histappend
shopt -s checkwinsize
# vim
export EDITOR='vim'
set -o vi
export VIMRUNTIME=/usr/local/share/vim/vim80
# Misc
export PAGER='less'
# end basic settings }}}
# aliases {{{

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Python
alias python=python3
alias pip=pip3
# Hylang
alias hy=hy3

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
#alias l='ls -CF'
alias vi="vim"

alias config='/usr/bin/git --git-dir=/home/datenstrom/.cfg/ --work-tree=/home/datenstrom'

# ./bin/bash

alias rd='reset-displays.sh'

# end aliases }}}
# path {{{
# Set .bin in PATH and it should be first
export PATH="$HOME/.bin/bash:$PATH"                     #  Personal Scripts
export PATH="$HOME/.local/bin:$PATH"                    #  pip local 
export PYTHONPATH="$HOME/.local/bin/:$HOME/.bin/python/:${PYTHONPATH}"
# Go path, for golangs scratch space
export GOPATH=$HOME/.go
export PATH=$GOPATH/bin:$PATH
# end path }}}
# program tweaking {{{
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# end program tweaking }}}
# prompt {{{
# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. ~/.local/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh
# end prompt }}}
# colors {{{

# Use 256color
export TERM=xterm-256color

# Solarized {{{
if tput setaf 1 &> /dev/null; then
    tput sgr0
    if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
      BASE03=$(tput setaf 234)
      BASE02=$(tput setaf 235)
      BASE01=$(tput setaf 240)
      BASE00=$(tput setaf 241)
      BASE0=$(tput setaf 244)
      BASE1=$(tput setaf 245)
      BASE2=$(tput setaf 254)
      BASE3=$(tput setaf 230)
      YELLOW=$(tput setaf 136)
      ORANGE=$(tput setaf 166)
      RED=$(tput setaf 160)
      MAGENTA=$(tput setaf 125)
      VIOLET=$(tput setaf 61)
      BLUE=$(tput setaf 33)
      CYAN=$(tput setaf 37)
      GREEN=$(tput setaf 64)
    else
      BASE03=$(tput setaf 8)
      BASE02=$(tput setaf 0)
      BASE01=$(tput setaf 10)
      BASE00=$(tput setaf 11)
      BASE0=$(tput setaf 12)
      BASE1=$(tput setaf 14)
      BASE2=$(tput setaf 7)
      BASE3=$(tput setaf 15)
      YELLOW=$(tput setaf 3)
      ORANGE=$(tput setaf 9)
      RED=$(tput setaf 1)
      MAGENTA=$(tput setaf 5)
      VIOLET=$(tput setaf 13)
      BLUE=$(tput setaf 4)
      CYAN=$(tput setaf 6)
      GREEN=$(tput setaf 2)
    fi
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # Linux console colors.
    # Not the Solarized values
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"
    BOLD=""
    RESET="\033[m"
fi

# End Solarized }}}


# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'


# get colors from ~/.dir_colors
eval "$(dircolors ~/.dir_colors)"

# end colors }}}
# command completion {{{

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
# end completion }}}
# Welcome Banner {{{

#cmatrix -s
# Date {{{
echo -en ${BLUE}
COLUMNS=$(tput cols)
time=$(date)
printf "%*s\n" $(((${#title}+$COLUMNS)/2)) "$time"
echo -e ${RESET}
# End Date }}}
# System Status {{{
if [ -x /usr/bin/neofetch ]; then
    neofetch --cpu_temp C \
             --disk_show '/', 'dev/sda', 'dev/sdb', 'dev/sdc' \
             --birthday_format "+%a %d %b %Y %H:%M"
elif [ -x /usr/bin/screenfetch ]; then
    screenfetch -c 13,4
elif [ -x /usr/bin/figlet ]; then
    echo -e "${CYAN} $(figlet -f small Welcome)\n$(figlet -f small $(whoami)!)"
    echo -en "${CYAN}This is BASH ${RED}${BASH_VERSION%.*}${CYAN} "
    echo -en "- Running on $(lsb_release -si) ${RED} $(lsb_release -sr) "
    echo -en "${CYAN} $(uname -o) ${RED}$(uname -r)\n"
    echo -en ${RESET}
fi
# End System Status }}}
# Cokkie {{{
if [ -x /usr/games/fortune ]; then
    echo -en ${BLUE}
    COLUMNS=$(tput cols)
    cookie=$(/usr/games/fortune -s)
    printf "%*s\n" $(((${#title}+$COLUMNS)/2)) "$cookie"
fi
# End Cokkie }}}
echo -en ${RESET} # Always Reset
# End Welcome }}}
