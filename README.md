# Home

**Deprecated in favor of [starshell/home](https://gitlab.com/starshell/home)**

This is my dotfile and system configuration backup.

&nbsp;
<p align="center">
  <img src="https://hyem.gitlab.io/home/home_demo.gif"></a>
</p>

&nbsp;
## There are already hundreds of these, why another?

tl;dr I don't like any of them.

> 13 Apr 2017
>
> My laptop died last week, and I had to restore the system from my backups. Having done this possibly thousands of times before I am just completely fed up with it. If I had invested all the effort I've wasted re-implementing my preferences throughout all my OS experimentation and refreshes I probably could have written my own personal shell by now.

So now system restoration is scripted. Hopefully I will never have to configure from scratch after a computer death again. Maybe now I will have time to write vimim and babash.

&nbsp;
## Summary

System configuration beyond dotfiles through install scripts currently only supports Debian based systems. Only tested on Debian itself.

* vim
    * Compiled latest release of vim with python 3 support
    * Code folding by marker
    * python-mode
    * Automatically set spell check by filetype
    * Automatically set indent by filetype
    * Iosevka font 
    * Airline status bar
    * Nerdtree directory traversal
    * Indent Guides
* bash
    * vim keybindings
    * Iosevka font
    * Information welcome banner
* Solarized Colors Everywhere
    * vim 
    * bash shell

## Making a new machine just like home

Back up any important dotfiles and setup the repository.

```bash
if ! grep -q -F "/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME" $HOME/.bashrc; then
    echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.bashrc
    source ~/.bashrc
fi

echo ".cfg" >> .gitignore

git clone --bare https://datenstrom@gitlab.com/datenstrom/home.git $HOME/.cfg

source ~/.bashrc

mkdir -p .config-backup && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}

config checkout

config config --local status.showUntrackedFiles no
```

Run the python script.

```bash
python ~/.bin/sysconfig/install.py
```

And change the terminal profile settings to use the _solarized_ color pallet and _losevka_ font. Finally run `:PlugInstall` in vim to install all of the plugins.
