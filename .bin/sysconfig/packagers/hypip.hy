#! /usr/bin/env hy
;;;;
;;;; hypy.hy
;;;; A wrapper for pip

(import pip)
(import shutil)

(defn pip-supported []
  "Determine if pip is supported"
  (not (is None (shutil.which "pip"))))

(defn pip-install [package &optional [root False] [args (list)]]
  "Install a package using pip"
  (setv install (+ ["install"] args [package]))
  (if root
    (pip.main (+ install))
    (pip.main (+ install ["--user"]))))

(defn pip-install-import [package &optional 
                         [root False]
                         [import-name None]
                         [args (list)]]
  "Install a required package then import it"
  (import importlib)
  (pip-install package :root root :args args)
  (setv name (if (not import-name) package import-name))
  (importlib.import_module name))

(defn pip-upgrade [package &optional [root False]]
  "Upgrade a package using pip"
  (setv install ["install" package "-U"])
  (if root
    (pip.main install)
    (pip.main (+ install ["--user"]))))

(defn pip-init []
  "Ensure a new pip installation is up to date"
  (pip-upgrade "pip")
  (pip-install (+
    "https://launchpad.net/python-distutils-extra/"
    "trunk/2.39/+download/python-distutils-extra-2.39.tar.gz"))
  (pip-upgrade "setuptools"))
