#! /usr/bin/env hy
(import shutil)
(import [hypip [pip-install-import]])


(defn apt-supported []
  "Determine if apt(-get) is supported"
  (setv apt (and 
    (not (is None (shutil.which "apt-cache")))
    (not (is None (shutil.which "apt-get")))))
  (setv dpkg (not (is None (shutil.which "dpkg"))))
  (and apt dpkg))


(defn apt-cache []
  "Speed up apt processing with python-apt"
  (setv args ["--no-cache-dir"])
  (try
    (import apt)
    (except [e ImportError] 
      ((if pip-supported
        (pip-install-import "python-apt" :import-name "apt" :args args))))
    (else
      (def cache (apt.Cache)))
    (finally (if pip-supported
      (pip-install-import "python-apt" :import-name "apt")
      (def cache (apt.Cache))))))


(defn apt-install [package &optional [root False] [source]]
  "Install a package using pip"
  (setv install ["install" package])
  (if root
    (pip.main install)
    (pip.main (+ install ["--user"]))))
