#!/bin/bash

repo='deb http://dl.bintray.com/dawidd6/neofetch jessie main'
key="https://bintray.com/user/downloadSubjectPublicKey?username=bintray"
curl -L "$key" -o Release-neofetch.key \
    && sudo apt-key add Release-neofetch.key \
    && rm Release-neofetch.key


if ! grep -q -F "$repo" /etc/apt/sources.list.d/mono-xamarin.list; then
    echo "$repo" | sudo tee -a /etc/apt/sources.list
fi


sudo apt-get update
sudo apt-get install neofetch
