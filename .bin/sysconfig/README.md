# sysconfig

Automated configuration of my preferred development environment.

## For a new machine

Before running the script setup `sudo` by first installing the command.

```bash
apt-get install sudo
```

Then adding the following to the `/etc/sudoers` file via `visudo`.

    <username> ALL=NOPASSWD: /usr/bin/apt-get, /usr/sbin/update-alternatives
    <username> ALL=NOPASSWD: /usr/bin/make
