#! /usr/bin/env hy


(defn confirm [question &optional [default None]]
  "Ask a yes or no question"
  (setv valid-answers {"yes" True "y" True "no" False "n" False})
  (cond
    [(is default None)
     (setv prompt " [y/n] ")]
    [(is default "yes")
      (setv prompt " [Y/n] ")]
    [(is default "no")
      (setv prompt " [y/N] ")])
  (print (+ "[*] " question prompt) :end "")
  (setv answer (.strip (.lower (input ))))
  (cond
    [(and (not (is default None)) (= answer ""))
     (get valid-answers default)]
    [(in answer valid-answers)
     (get valid-answers answer)]
    [True
     (do (print "[*] Please respond with either (yes/y) or (no/n)")
         (confirm question default))]))


(defn sha1sum [path]
  "Return the SHA1 sum of a file at path"
  (import hashlib)
  (setv hash_sha1 (hashlib.sha1 (.encode "")))
  (with [f (open path "rb")]
    (for (buff (iter (fn [] (.read f 4096)) (.encode "")))
      (hash_sha1.update buff)))
  (hash_sha1.hexdigest))
