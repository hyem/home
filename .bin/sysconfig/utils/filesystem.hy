(import os)
(from glob (import glob))


(defn touch [path]
  "Create path and file if it does not exist"
  (setv basedir (os.path.dirname path))
  (if (not (os.path.exists basedir))
      (os.makedirs basedir))
  (with [f (open path "a")]
    (os.utime path None)))


(defn lines [path]
  "Get lines of a file as a list"
  (with [f (open path "r")]
    (list-comp (.strip line) (line (.readlines f)))))


(defn files [path]
  "Get all files in a directory as a list"
  (glob path))
